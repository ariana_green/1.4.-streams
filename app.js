"use strict";

const fs = require('fs');
const crypto = require('crypto');
const input = fs.createReadStream('data.txt');

//---------- PART 1 ---------------------
const output1 = fs.createWriteStream('result1.txt');
const hash1 = crypto.createHash('md5');

hash1.on('finish', () => {
	let result = hash1.read().toString('hex');
	console.log('Output1: ', result);
	output1.write(result);
	output1.end();
})

input.pipe(hash1);

//---------- PART 2 ----------------------
const Transform = require('stream').Transform;
const output2 = fs.createWriteStream('result2.txt');

class CTransform extends Transform {
	constructor(options){
		super(options);
	}
	_transform(data, encoding, cb){
    cb(null, data.toString('hex'));
	}
}
const my1 = new CTransform();
const my2 = new CTransform();

input.pipe(crypto.createHash('md5')).pipe(my1).pipe(output2);
input.pipe(crypto.createHash('md5')).pipe(my2).pipe(process.stdout);
